package  
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * カオス棒作るよ
	 * @author world
	 */
	public class ChaosBou extends Sprite
	{
		private var bmp_ue:Bitmap;
		private var bmp_shita:Bitmap;
		
		public var clearPointY:int;
		
		public static const DEFAULT_SPEED:Number = -7.0;
		public var DEFAULT_POSITION:Point;
		public static const DEFAULT_OASISU_SPEED:Number = 1.5;
		
		public var speed:Number;
		public var oasisuHeight:int;
		public var oasisuSpeed:Number;

		
		public function ChaosBou() 
		{
			bmp_ue = new Images.ImageChaosbou as Bitmap;
			bmp_shita = new Images.ImageChaosbou as Bitmap;
			
			init();
			
			addChild(bmp_ue);
			addChild(bmp_shita);
		}
		
		private function init():void 
		{
			oasisuHeight = 133;
			clearPointY = 200;
			
			setChaosBou();
			
			speed = DEFAULT_SPEED;
			oasisuSpeed = DEFAULT_OASISU_SPEED;
		}
		
		public function setChaosBou():void 
		{
			bmp_ue.y = clearPointY - bmp_ue.height;
			bmp_shita.y = clearPointY + oasisuHeight;
		}
		
		public function testHit(obj:DisplayObject):Boolean
		{
			if (bmp_ue.hitTestObject(obj) || bmp_shita.hitTestObject(obj)) return true;
			else return false;
		}
	}

}