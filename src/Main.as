package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ドキュメントクラスだよ
	 * @author world
	 */
//	[SWF(width = "780", height = "512", backgroundColor = "0xFFFFFF", frameRate = "24")]
//	[SWF(width = "1366", height = "768", backgroundColor = "0xFFFFFF", frameRate = "24")]
	[SWF(width = "1366", height = "768", backgroundColor = "0xFFFFFF", frameRate = "24")]
	public class Main extends Sprite 
	{
		private var irairakun:Irairakun;
		private var chaosbou:ChaosBou;
		private var scoreBoard:ScoreBoard;
		private var titleField:TitleField;
		private var bgm:SoundPlayer;
		private var se1:SoundPlayer;
		private var se2:SoundPlayer;
		
		private var bg_shape:Shape;//背景用オブジェクト
		
		private var isRunning:Boolean;
		
		private static const STAGE_WIDTH:int= 1366;
		private static const STAGE_HEIGHT:int = 768;
		
		private var friction:Number = 0.015;
		
		public function Main():void 
		{
			stage.scaleMode = "noScale";
			// 中央指定
			stage.align = "";
			//ｂｇｍ
			bgm = new SoundPlayer("data/mp3/Sleep Away.mp3", 100);
			se1 = new SoundPlayer("data/mp3/botan_b33.mp3", 1);
			se2 = new SoundPlayer("data/mp3/danmatsuma.mp3", 1);

			if (stage) {
				init();
				if (stage.displayState == "normal")
					stage.displayState = "fullScreen"; // フルスクリーン化
				else
					stage.displayState = "normal"; // 通常化
			}		
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point			
			//いらいら君を置く
			irairakun = new Irairakun();
			irairakun.y = STAGE_HEIGHT / 2.0;
			irairakun.x = 240;
			stage.addChild(irairakun);
			
			//カオス棒を置く
			chaosbou = new ChaosBou();
			chaosbou.x = STAGE_WIDTH;
			chaosbou.y = 0;
			stage.addChild(chaosbou);
			
			//得点ボードを置く
			scoreBoard = new ScoreBoard();
			scoreBoard.x = STAGE_WIDTH - 1 * scoreBoard.width;
			scoreBoard.y = 10;
			stage.addChild(scoreBoard);
			
			//タイトルを置く
			titleField = new TitleField();
			titleField.x = 600;
			titleField.y = 200;
			stage.addChild(titleField);
			
			change_backColor(0xFFFFFF);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		/**
		 * マウスが動かされた際呼ばれるメソッドです
		 * @param	e
		 */
		private function onMouseMove(e:MouseEvent):void 
		{
			irairakun.speedY = (e.stageY - irairakun.y) * friction;
			irairakun.speedX = (e.stageX - irairakun.x) * friction;
		}
		/**
		 * 毎フレーム呼ばれるメソッドです
		 * @param	e
		 */
		private function onEnterFrame(e:Event):void 
		{
			//いらいらくん
			irairakun.x += irairakun.speedX;
			irairakun.y += irairakun.speedY;
			if (irairakun.x < 0) irairakun.x = 0;
			else if (STAGE_WIDTH - irairakun.width < irairakun.x) irairakun.x = STAGE_WIDTH - irairakun.width;
			if (irairakun.y < 0) irairakun.y = 0;
			else if (STAGE_HEIGHT - irairakun.height < irairakun.y) irairakun.y = STAGE_HEIGHT - irairakun.height;
			
			//カオス棒
			chaosbou.x += chaosbou.speed;
			chaosbou.clearPointY += chaosbou.oasisuSpeed;
			chaosbou.setChaosBou();
			if (chaosbou.clearPointY <= 0 || STAGE_HEIGHT - chaosbou.oasisuHeight <= chaosbou.clearPointY) chaosbou.oasisuSpeed *= -1;
			if (chaosbou.x < 0 - chaosbou.width) {
				chaosbou.x = STAGE_WIDTH;
				scoreBoard.score ++;
				scoreBoard.setText();
				
				switch(scoreBoard.score) {
					case 2:
						chaosbou.oasisuHeight = 128;
					case 5:
						chaosbou.oasisuHeight = 123;
						break;
					case 10:
						chaosbou.oasisuHeight = 118;
						change_backColor(0xFF00FF);
						break;
					case 15:
						change_backColor(0x00FFFF);
						break;
					case 20:
						change_backColor(0x002200);
						break;
					case 25:
						change_backColor(0xFFE0FF);
						break;
				}

				chaosbou.oasisuSpeed *= (scoreBoard.score % 4) ? 1.0:1.1;
				chaosbou.speed = (ChaosBou.DEFAULT_SPEED - (scoreBoard.score / 5)) * (Math.random() + 0.5);
			}
			
			//GAME OVER
			if (chaosbou.testHit(irairakun)) {
				bgm.stop();
				if(scoreBoard.score >= 20)
					se2.play();
				else
					se1.play();
				stage.removeChild(irairakun);
				stage.removeChild(chaosbou);
				stage.removeChild(scoreBoard);
				
				init();
				stage.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			
		}
		private function onClick(e:MouseEvent):void 
		{
			stage.removeChild(titleField);
			stage.removeEventListener(MouseEvent.CLICK, onClick);
			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);				
			bgm.play();
		}
		private function change_backColor(col:int):void 
		{
			if(bg_shape)
				removeChild(bg_shape);
			//背景色用矩形
			bg_shape = new Shape();
			bg_shape.graphics.beginFill(col);//塗りつぶしカラー(RGB値:0xRRGGBB形式)
			bg_shape.graphics.drawRect(0,0,STAGE_WIDTH,STAGE_HEIGHT);//矩形(X座標,Y座標,幅,高さ)
			addChildAt(bg_shape,0);//表示リストレイヤー0に矩形を追加
		}
	}
	
}