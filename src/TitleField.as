package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author world
	 */
	public class TitleField extends Sprite
	{
		private var txtField:TextField;
		
		public function TitleField() 
		{
			var format:TextFormat = new TextFormat();
			format.bold = true;
			format.size = 63;
			
			txtField = new TextField();
			txtField.defaultTextFormat = format;
			txtField.text = "  ###いらいらくん###\n\n\n\n＜「くりっくしてください」";
			
			format.size = 32;
			txtField.setTextFormat(format, 14, txtField.text.length);
			
			txtField.autoSize = "center";
						
			addChild(txtField);
		}
		
	}

}