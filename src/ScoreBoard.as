package  
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author world
	 */
	public class ScoreBoard extends Sprite
	{
		public var score:int;
		private var scoreBoard:TextField;
		private static var maxScore:int = 0;
		
		public function ScoreBoard() 
		{
			init();
			
			var format:TextFormat = new TextFormat();
			format.bold = true;
			format.size = 34;
			
			scoreBoard = new TextField();
			scoreBoard.defaultTextFormat = format;
			setText();
			scoreBoard.autoSize = "right";
						
			addChild(scoreBoard);
		}
		
		private function init():void 
		{
			score = 0;
		}
		
		public function setText():void 
		{
			if (score > maxScore) maxScore = score;
			scoreBoard.text = "#とくてん  ："+ score.toString() + "#\n#いちばん：" + maxScore.toString() + "#";
		}
	}

}