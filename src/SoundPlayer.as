package  
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author world
	 */
	public class SoundPlayer
	{
		private var sound_obj :Sound;
		private var roop:int;
		private var channnel:SoundChannel;
		
		public function SoundPlayer(urlStr:String, roops:int) 
		{			
			sound_obj = new Sound();
			setSound(urlStr, roops);
			channnel = null;
		}
		
		public function play():void 
		{
			if(sound_obj)
			channnel = sound_obj.play(0,roop);
		}
		public function stop():void 
		{
			if(channnel)
			channnel.stop();
		}
		private function setSound(urlStr:String, roops:int):void 
		{
			var url:URLRequest = new URLRequest(urlStr);
			sound_obj.load(url);;			
			roop = roops;
		}
	}

}